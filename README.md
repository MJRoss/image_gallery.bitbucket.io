# README #

### What is this repository for? ###

* A responsive image gallery with animations and transitions
* Version 1.0

### Technologies Used ###

* HTML5, CSS3, and JavaScript.
* Employs Unsplash API for gallery images

### How To Use This Repo ###

* Navigate to hosted version of repo by clicking on the link at the bottom
* Hover over images to see an overlay animation
* Click the "Travel" header to see the globe image animation

### Repository Owner ###

* Malcolm Ross

### Link To Hosted Version ###
* https://malcolmross19.github.io/image_gallery.bitbucket.io/
